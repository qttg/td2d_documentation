###############
The Spins class
###############

Creating a uniform 2D array of spins
====================================

We start by creating a tight-binding system to which we want to 
bind the classical spins (magnetic moments). In order to simplify
the building process, TD2D uses KWANT tight-binding object, so one
usually has to start with building their system using KWANT.
However, TD2D already offers a simple function
to build a simple 2D ribbon using KWANT.


.. code-block:: python3
  
   from td2d.kwant_systems import make_ribbon
   ribbon = make_ribbon(length=5, width=4)

After we created the ribbon, we import the classical ``Spins``
class from TD2D

.. code-block:: python3

    from td2d import Spins

This is the class that will hold our classical magnetic moments.
After importing it, we can build the classical spins by providing
the KWANT tight-binding object as a single required argument to
which we want to bind the spins. Note that the present version
only works for 2D systems, so do not try to create a multilayer
system and pass it to the ``Spins`` class

.. code-block:: python3

   cspins = Spins(ribbon)

Currently, the spins are created on every site of the KWANT tight
binding system. If we want to see how the created system looks like,
we can plot it using

.. code-block:: python3

   cspins.plot(filename='spins_3d.png') 

or 

.. code-block:: python3
   
   cspins.plot(plot_type='2d', filename='spins_3d.png') 

Currently, spins can be plotted using Matplotlibs 3D plotting 
features, using Axes3D objects,
(set by default, option ``plot_type='3d'``), although they don't
provide quite nice looking plots (see below). 
Another plotting option is using only in-plane :math:`xy` 
projections of local moments, while :math:`z` is represented by
color (``plot_type='2d'``), or using Mayavi plotting library 
(option ``plot_type='mayavi'``). Note that in the last case,
Mayavi library needs to be installed and run on a local machine 
(plotting on a remote machine doesn't work currently, but it is
possible to fix it in the future). Currently, the plotting function
is lacking the ability to set the camera view in 3D, but this
feature will be added shortly.  Also, note that beside plotting 
to a local file, you can also pass an appropriate Matplotlib Axes
object using ``ax`` keyword argument to ``cspins.plot()`` 
function (for plot types ``2d`` and ``mayavi`` you pass
a regular Matplotlib's Axes object, while for ``3d`` you need Axes3D
object). 

.. figure:: ../figs/spins_plot_combine.png
   :align: center
   :width: 60 %
    
   Two out of three possible ways to plot local magnetic moments
    

The ``Spins`` class currently doesn't have any options to save
spins, but they are kept inside this class ``s`` attribute, and
therefore can be accessed and saved easly, both in txt or npz format

.. code-block:: python3

   import numpy as np
   np.savetxt('cspins.txt', cspins.s)
   np.savez('cspins.npz', cspins=cspins.s)

Spins are kept in atribute ``cspins.s`` as a :math:`(N, 3)` 
numpy array, where :math:`N` is the number of sites, and the second
axis is for three components :math:`x`, :math:`y`, and :math:`z`.
The ordering of the spin sites is the same as the ordering of
lattice sites in the KWANT's ``ribbon`` object that we passed when
we created the ``Spins`` object. To check their actual positions,
you can use their ``pos_spins`` attribute, which contains a 
:math:`(N, 2)` numpy array of spin positions in the :math:`x` and
:math:`y` direction.

.. code-block:: python3

   print(cspins.pos_spins)

Creating a custom spin configuration
====================================

By default, all classical moments are pointing in the same direction
(along the :math:`z` axis). There are two ways to control the
initial spin orientation. The first is to provide a tuple with 
three floats to ``spin_config`` keyword argument, like in the
following example

.. code-block:: python3

   from td2d import Spins
   from td2d.kwant_systems import make_ribbon

    ribbon = make_ribbon(length=10, width=4)
    cspins = Spins(ribbon, spin_config=(1., 1, 1))
    cspins.plot(plot_type='3d', filename='spins.png')

The created ribbon is shown on the folowing figure. Note that
although the provided tuple was not a unit vector, when local
spins are created, they are normalized. In other words,
``spin_config`` is providing just a direction vector for all spins,
and it will always create a uniform configuration. 

.. figure:: ../../source/figs/tilted_spins.png
   :align: center
   :width: 40%

   Uniform configuration of tilted spins obtained with
   ``spin_config=(1., 1., 1)`` keyword argument.

In most of the use cases, we won't deal with uniform configurations
of local spins. Instead, we will need to configure some noncollinear
spin texture. The TD2D package is allowing us to do that in a way
that is very similar to KWANT methods for building the system, 
but now for 3D vectors. In order to make a custom spin
configuration, one has to define a python function which accepts
a single KWANT ``Site`` object and returns a 3D vector. For example,
here is how one would define a simple magnetic domain wall using 
TD2D. Firstly, one would define a function 

.. code-block:: python3

   import numpy as np
   from math import cosh, tanh

   def ohe_dw(site, x_zero=5, width=2):
        x, y = site.pos
        spin = np.array([1.0/cosh((x_zero-x)/width), 0,
                     tanh((x_zero-x)/width)])
        return spin

And then, it would provide this function to ``config`` keyword
argument of the TD2D ``Spins`` object.

.. code-block:: python3

   from td2d import Spins
   from td2d.kwant_systems import make_ribbon

   ribbon = make_ribbon(length=10, width=4)
   cspins = Spins(ribbon, config=ohe_dw)
   cspins.plot(plot_type='mayavi', filename='domain_wall.png')


Note that provided function has only one required argument, but
user can set arbitrary number of keyword arguments to it. This
building mechanism is somewhat similar to KWANT's mechanism of
building a tight-binding system. In this case, the ``Spins`` class
iterates through all the ``Site`` objects of the provided KWANT
tight-binding system, calls the provided ``config``  function
on each site, and computes the value of spin for that site. Finally,
we can see how the constructed domain wall looks like (see the
figure on the next page).

.. figure:: ../../source/figs/domain_wall.png
   :align: center
   :width: 40%
    
   A simple domain wall created by providing a configuration
   function to TD2D ``Spins`` class ``config`` 
   keyword argument.
