
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>1. Introduction to TD2D &#8212; td2d 0.0.1 documentation</title>
    <link rel="stylesheet" href="../_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script src="../_static/language_data.js"></script>
    <script crossorigin="anonymous" integrity="sha256-Ae2Vz/4ePdIu6ZyI/5ZGsYnb+m0JlOmKPjt6XZ9JJkA=" src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="2. The Spins class" href="llg.html" />
    <link rel="prev" title="TD2D Documentation" href="../index.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="llg.html" title="2. The Spins class"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="../index.html" title="TD2D Documentation"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../index.html">td2d 0.0.1 documentation</a> &#187;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="introduction-to-td2d">
<h1><span class="section-number">1. </span>Introduction to TD2D<a class="headerlink" href="#introduction-to-td2d" title="Permalink to this headline">¶</a></h1>
<p>TD2D is a Python package for simulating time-dependent
quantum transport in open 2D systems,
with an optional coupling to classical magnetic moments.
TD2D can be viewed as an extension (or
an add-on) to a widely used KWANT package, since it is
mostly based and operates with objects defined in KWANT.</p>
<p>In this part we give a basic introduction to TD2D.
We start by presenting its main classes and
how they interact with each other. In the following
chapters, we will go into details of explaining each class
separately.</p>
<div class="section" id="organisation-of-the-td2d-package">
<h2><span class="section-number">1.1. </span>Organisation of the TD2D package<a class="headerlink" href="#organisation-of-the-td2d-package" title="Permalink to this headline">¶</a></h2>
<div class="section" id="main-classes-and-workflow">
<h3><span class="section-number">1.1.1. </span>Main classes and workflow<a class="headerlink" href="#main-classes-and-workflow" title="Permalink to this headline">¶</a></h3>
<p>The following figure gives us a schematic view of the TD2D
package. It consists mostly out of two main classes
which are the <code class="docutils literal notranslate"><span class="pre">Device</span></code> class, and the <code class="docutils literal notranslate"><span class="pre">Spins</span></code> class.</p>
<div class="figure align-center" id="id1">
<a class="reference internal image-reference" href="../_images/td2d_scheme.png"><img alt="../_images/td2d_scheme.png" src="../_images/td2d_scheme.png" style="width: 40%;" /></a>
<p class="caption"><span class="caption-text">The main classes of the TD2D package and how they are
related to each other. Black arrows show how data flows
between class instances. Red arrows shows which class
instance requires which in order to get build.</span><a class="headerlink" href="#id1" title="Permalink to this image">¶</a></p>
</div>
<p>Since TD2D is an extension of the KWANT package, we don’t
build our system from scratch. Instead, we start from
KWANT’s tight-binding system, that is an
instance of the <code class="docutils literal notranslate"><span class="pre">FiniteSystem</span></code> class in KWANT. This
system is represented as <code class="docutils literal notranslate"><span class="pre">TB</span> <span class="pre">System</span></code> in our previous
diagram. TD2D currently assumes that the system is attached
to at least one lead, and that all leads are made from
square lattices. This limitation is imposed on us by the
assumptions of equations implemented in our time dependent
solver. Although this restricts devices that can be
simulated to only those where electrons are injected
from square lattice leads, there are no limitations
on the nature of tight-binding Hamiltonian in the main
scattering region.</p>
<p>As we said, we start by building KWANT’s <code class="docutils literal notranslate"><span class="pre">FiniteSystem</span></code>
object with leads attached to it.
Since TD2D allows for self-consistent simulation of both
classical magnetic moments and quantum spin-polarized
electrons in an open system, the two main classes
(the <code class="docutils literal notranslate"><span class="pre">Spins</span></code> class and the <code class="docutils literal notranslate"><span class="pre">Device</span></code> class).
serve that purpose. Note that they are both constructed from
the KWANT’s <code class="docutils literal notranslate"><span class="pre">FiniteSystem</span></code>, that is <code class="docutils literal notranslate"><span class="pre">TB</span> <span class="pre">System</span></code> in our
notation (see red arrows (1) and (2) in the TD2D schematic
figure). By providing the same instance of the KWANT
system to builders of these two classes, we allow for the
same site indexing in both classes. We also use KWANT
functionality in creating systems with parametric
Hamiltonians, its solvers for lead selfenergies, and
its ability to search for neighboring sites.</p>
<p>The Device class is basically an extension of the
<code class="docutils literal notranslate"><span class="pre">FiniteSystem</span></code> class with few extra attributes and
functions to allow for simpler manipulation of
time-dependent Hamiltonians.  Since KWANT can create
parametrized tight-binding systems,
it is important to specify how TD2D implements
time-varying parametric Hamiltonians. Beside a tuple of
constant argument values (how they are
usually supplied in KWANT) TD2D allows for
argument values which are functions of a single variable
(time), and these functions need to be supplied to the
<code class="docutils literal notranslate"><span class="pre">Device</span></code> class during its construction (as an argument
list). This also applies
to the time-dependent voltage signals that are to be applied
to the system leads (see the
two green rectangles for <img class="math" src="../_images/math/14f55ba5b06ad8e125f968ffd194002dbd165540.svg" alt="{\rm V(t)}"/> and <img class="math" src="../_images/math/80f1f577a6ad9dbec9c9d020237474800215bbd8.svg" alt="{\rm
Args(t)}"/> in the TD2D schematics). The purpose of the
device class it intermediary, to facilitate creation of
the time-dependent Hamiltonians, and to pass those
Hamiltonians to time evolution solvers.</p>
<p>The time-evolving internal state of the tight-binding system
can be described either by its wave function,
or by it’s density matrix.
The <code class="docutils literal notranslate"><span class="pre">Device</span></code> class doesn’t contain this data, but instead
we implement it in the <code class="docutils literal notranslate"><span class="pre">TD-NEGF</span> <span class="pre">Solver</span></code> class. There are
several different implementations of this class, as we’ll
see later. This is partly because there are several ways to
implement the time evolution (both mathematically,
and in the code), and, as we said before, each method will
require different representation of the
system state.  The <code class="docutils literal notranslate"><span class="pre">Device</span></code> class is necessary to
initialize the time evolution object
(see the red arrow (3) in the TD2D schematics).
Additionally, the TD2D package (by default) creates an
equilibrium solver object and attaches it to the <code class="docutils literal notranslate"><span class="pre">Device</span></code>
class.
The purpose of both equilibrium and nonequilibrium solver
is to compute the corresponding density matrix for each
time step when the <code class="docutils literal notranslate"><span class="pre">Device</span></code> class supplies the
time-dependent Hamiltonians to them.</p>
<p>In the last step, there are objects which represent
different measurement operators. There are some default
measurements objects necessary for the consistency of the
entire package (e.g. local electronic spin density operator).
Others can be defined by the user, depending on which
operator he wants to measure.</p>
<p>Since TD2D works with both electrons and classical magnetic
moments, the <code class="docutils literal notranslate"><span class="pre">Spins</span></code> class is responsible for time
evolution of the later. It
needs to be constructed using the <code class="docutils literal notranslate"><span class="pre">TB</span> <span class="pre">System</span></code>
(red arrow (2) in the TD2D schematics) independently from
the <code class="docutils literal notranslate"><span class="pre">Device</span></code> class. TD2D can be viewed as a collection
of two independent packages. The <code class="docutils literal notranslate"><span class="pre">Spins</span></code> class has its
own methods for manipulating and propagating local
magnetic moments in time, based on the
Landau-Lifshitz-Gilbert (LLG) time evolution solver.</p>
<p>Although, as we said, the two classes (<code class="docutils literal notranslate"><span class="pre">Spins</span></code> and
<code class="docutils literal notranslate"><span class="pre">Device</span></code>) can be propagated independently from each other,
they can also be propagated in a self-consistent loop, where
<code class="docutils literal notranslate"><span class="pre">Spins</span></code> object modifies the local onsite energy
(<img class="math" src="../_images/math/3b96058a5543cc4001c02996ea4cad055f67042f.svg" alt="{\Delta U(t)}"/>) of the
sites where the spins are attached to the electronic system,
and electrons can influence the spin dynamics through their
nonequilibrium (current-driven) spin density <img class="math" src="../_images/math/7cbefdd549ee61eeebad6bb28439c5a465cfeb9a.svg" alt="\langle
\hat{\mathbf{s}}_{\rm neq}(t)\rangle -
\langle\hat{\mathbf{s}}_{\rm
eq}(t)\rangle"/>.</p>
</div>
</div>
<div class="section" id="how-to-read-the-rest-of-this-manual">
<h2><span class="section-number">1.2. </span>How to read the rest of this manual<a class="headerlink" href="#how-to-read-the-rest-of-this-manual" title="Permalink to this headline">¶</a></h2>
<p>In the next two chapters we describe how to
create and use the <code class="docutils literal notranslate"><span class="pre">Spins</span></code> class objects. In
chapters 4 and 5 we describe the <code class="docutils literal notranslate"><span class="pre">Device</span></code> objects,
separately.
This is for users who want to perform either only
classical micromagnetics using LLG or only quantum
electron dynamics using time-dependent non-equilibrium
Green function formalism (TD-NEGF). In chapter 6,
we describe how to combine the two in a self-consistent
way (what we refer as TD-NEGF+LLG method).</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="../index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">1. Introduction to TD2D</a><ul>
<li><a class="reference internal" href="#organisation-of-the-td2d-package">1.1. Organisation of the TD2D package</a><ul>
<li><a class="reference internal" href="#main-classes-and-workflow">1.1.1. Main classes and workflow</a></li>
</ul>
</li>
<li><a class="reference internal" href="#how-to-read-the-rest-of-this-manual">1.2. How to read the rest of this manual</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="../index.html"
                        title="previous chapter">TD2D Documentation</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="llg.html"
                        title="next chapter"><span class="section-number">2. </span>The Spins class</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/chapters/intro.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="llg.html" title="2. The Spins class"
             >next</a> |</li>
        <li class="right" >
          <a href="../index.html" title="TD2D Documentation"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../index.html">td2d 0.0.1 documentation</a> &#187;</li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, QTT Group, University of Delaware.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.4.1.
    </div>
  </body>
</html>